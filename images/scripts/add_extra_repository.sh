#!/bin/bash
# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

# Add an extra apt repository sources.list if the extra-repository env vars were
# set.

TARGET_ETC="/etc"
VERBOSE=0

while [[ "$#" -ge 1 ]]; do
    case "$1" in
        --target-etc|-t)
            shift
            TARGET_ETC="$1"
            shift
            ;;
        --verbose|-v)
            VERBOSE=1
            shift
            ;;
    esac
done

mkdir -p "${TARGET_ETC}"/apt/sources.list.d/
index=0
list="${SALSA_CI_EXTRA_REPOSITORY}"
while [ -n "${list}" ]; do
    list="$(echo "${list}" | grep -v '^ *\(#.*\)\?$')"
    case "${list}" in
    "deb "*|"deb-src "*)
        echo "${list}" >> "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
        ;;
    *)
        if [ -n "${list}" ]; then
            cat "${list}" >> "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
        fi
        ;;
    esac

    index=$((index+1))
    name=SALSA_CI_EXTRA_REPOSITORY_${index}
    list="${!name}"
done

if [[ "$VERBOSE" -ne 0 ]] && [[ $index -ne 0 ]]; then
    cat "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
fi

mkdir -p "${TARGET_ETC}"/apt/trusted.gpg.d/
index=0
key="${SALSA_CI_EXTRA_REPOSITORY_KEY}"
keyring="${TARGET_ETC}"/apt/trusted.gpg.d/extra_repository.gpg
while [ -n "${key}" ]; do
    touch "${keyring}"
    if { echo "${key}"; } | grep -q 'BEGIN PGP PUBLIC KEY BLOCK'; then
        echo "${key}" | apt-key --keyring "${keyring}" add -
    else
        for fname in $(echo "${key}" | grep -v '^ *\(#.*\)\?$'); do
            case "${fname##*.}" in
            asc|gpg)
                apt-key --keyring "${keyring}" add "${fname}"
                ;;
            *)
                echo "Unsupported key: ${fname}" >&2; exit 1;;
            esac
        done
    fi

    index=$((index+1))
    name=SALSA_CI_EXTRA_REPOSITORY_KEY_${index}
    key="${!name}"
done

if [[ "$VERBOSE" -ne 0 ]] && [[ $index -ne 0 ]]; then
    gpg --no-default-keyring \
        --keyring "${TARGET_ETC}"/apt/trusted.gpg.d/extra_repository.gpg \
        --list-keys
fi

test "${TARGET_ETC}" != "/etc" || apt-get update
