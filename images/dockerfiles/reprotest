# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ARG RELEASE
ARG LABEL_IMAGE_TITLE
ARG LABEL_IMAGE_DESC

LABEL org.opencontainers.image.title="${LABEL_IMAGE_TITLE}" \
	org.opencontainers.image.description="${LABEL_IMAGE_DESC}"

RUN apt-get update \
	&& eatmydata apt-get install --no-install-recommends --yes \
		reprotest \
		patch \
		faketime \
		locales-all \
		disorderfs \
		sudo \
		xxd \
		unzip \
	&& rm -rf /var/lib/apt

COPY patches/reprotest/* /tmp/
RUN PATCH=/tmp/build.patch.${RELEASE}; \
    [ -e "$PATCH" ] || PATCH=/tmp/build.patch; \
    patch -p1 < ${PATCH}

ENV PYTHONIOENCODING utf-8
