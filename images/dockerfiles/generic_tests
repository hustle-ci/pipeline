# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ARG LABEL_IMAGE_TITLE
ARG LABEL_IMAGE_DESC

LABEL org.opencontainers.image.title="${LABEL_IMAGE_TITLE}" \
	org.opencontainers.image.description="${LABEL_IMAGE_DESC}"

RUN \
    set -eux; \
    apt-get update; \
    \
# install dependencies for check rc bugs
    if [ -n "$(apt-cache search --names-only ^python3-junit\\.xml\$)" ]; then \
        eatmydata apt-get install --no-install-recommends --yes \
            python3-debianbts \
            python3-junit.xml \
    ; \
    fi; \
# install dependencies for check for missing replaces
    eatmydata apt-get install --no-install-recommends --yes \
        apt-file \
        python3-debian \
    ; \
    if [ -z "$(apt-cache search --names-only ^python3-junit\\.xml\$)" ]; then \
        eatmydata apt-get install --no-install-recommends --yes \
            python3-pip \
        ; \
        pip3 install junit-xml; \
    else \
        eatmydata apt-get --no-install-recommends --yes install \
            python3-junit.xml \
        ; \
    fi; \
# install dependencies for blhc
    eatmydata apt-get install --no-install-recommends --yes \
        blhc \
    ; \
    rm -rf /var/lib/apt; \
    :

COPY scripts/check_rc_bugs.py /usr/local/bin/
COPY scripts/check_for_missing_breaks_replaces.py /usr/local/bin/
